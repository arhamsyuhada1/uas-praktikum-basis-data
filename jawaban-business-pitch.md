# Jawaban Nomor 1
### Use case

![use_case](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/usecase%20gofood.drawio.png)

# Jawaban Nomor 2
### Database Connection

![db_con_1](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_2/(1)%20db%20conn.png)

![db_con_2](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_2/(2)%20db%20conn.png)

# Jawaban Nomor 3
### Visualisasi Data via Metabase

![visualisasi_data](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_3/Screenshot%202023-07-15%20130917.png)

# Jawaban Nomor 4
### Built-in func REGEXP

![built_in_regexp](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_4/regexp.png)

### Built-in func SUBSTRING

![built_in_substring](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_4/substring.png)

### Built-in func tambahan (AVG)

![built_in_avg](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_4/avg.png)


# Jawaban Nomor 5
### Subquery

![subquery](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_5/subquery.png)

# Jawaban Nomor 6
### Transaction

![transaction](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_6/transaction.png)

# Jawaban Nomor 7

# Jawaban Nomor 8

# Jawaban Nomor 9
### Foreign key

![foreign_key](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_9/foreign-key.png)

### Index

![index](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_9/index%20and%20unique.png)

### Unique

![unique](https://gitlab.com/arhamsyuhada1/uas-praktikum-basis-data/-/raw/main/img/uas_no_9/index%20and%20unique.png)

# Jawaban Nomor 10

# Jawaban Nomor 11
